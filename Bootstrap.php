<?php declare(strict_types=1);

namespace Plugin\jtl_no_install_sunday;

use JTL\Alert\Alert;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;

/**
 * Class Bootstrap
 * @package Plugin\jtl_no_install_sunday
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function preInstallCheck(): bool
    {
        if ((int)\date('w') !== 0) {
            return true;
        }
        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_DANGER,
            'Sie können dieses Plugin nicht an einem Sonntag installieren!',
            'jtlnoinstallsundaymsg'
        );

        return false;
    }
}
